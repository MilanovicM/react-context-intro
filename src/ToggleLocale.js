import React from "react";
import { LocaleContext } from "./context/LocaleContext";

export default () => {
    return (
        <LocaleContext.Consumer>
            {localeValue => (
                <button
                    onClick={localeValue.changeLocale}
                >Change Language
            </button>
            )}
        </LocaleContext.Consumer>
    );
};