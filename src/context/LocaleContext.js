import React from "react";

// react creates new context, we can pass optional default value
export const LocaleContext = React.createContext();

// createContext(defaultValue) returns Provider and Consumer component

class LocaleProvider extends React.Component {
    constructor(props) {
        super(props);

        this.changeLocale = () => {
            this.setState(state => {
                const newLocale = state.locale === "en" ? "fr" : "en";
                return {
                    locale: newLocale
                };
            });
        };

        this.state = {
            locale: "en",
            changeLocale: this.changeLocale
        }
    };

    render() {
        return (
            <LocaleContext.Provider value={this.state}>
                {this.props.children}
            </LocaleContext.Provider>
        );
    };
};

export default LocaleProvider;
