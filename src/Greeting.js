import React from "react";
import { LocaleContext } from "./context/LocaleContext";

export const Greeting = props => (
    <LocaleContext.Consumer>
        {localeVal => localeVal.locale === "en" ? <h2>Welcome!</h2> : <h2>Bienvenue!</h2>}
    </LocaleContext.Consumer>
);